package com.control.back.halo.manage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.control.back.halo.basic.controller.BaseController;
import com.control.back.halo.manage.entity.FixCode;
import com.control.back.halo.manage.service.IFixCodeService;

@Controller
@RequestMapping("/fixcode")
public class FixCodeController extends BaseController {

    @Autowired
    private IFixCodeService fixCodeService;

    @RequestMapping({ "", "index" })
    public String index(Model model) {
        model.addAttribute("fixcodes", fixCodeService.findAll());
        return "fixcode/index";
    }
    
    @RequestMapping("/queryById/{id}")
    @ResponseBody
    public ResponseEntity<FixCode> queryById(@PathVariable("id") Long id){
        return ResponseEntity.ok(fixCodeService.find(id));
    }

    @RequestMapping("/remove")
    public String remove(Long id) {
        fixCodeService.delete(id);
        return "redirect:index.html";
    }

    @RequestMapping("/saveOrUpdate")
    public String saveOrUpdate(FixCode fixcode) {
        fixCodeService.saveOrUpdate(fixcode);
        return "redirect:index.html";
    }
}
