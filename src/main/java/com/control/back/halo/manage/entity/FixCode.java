package com.control.back.halo.manage.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import com.control.back.halo.basic.entity.BaseEntity;

/**
 * 实体类TFixCode
 * 
 * @author saic-tools-generator
 */
@Entity
@DynamicInsert
@DynamicUpdate
@Table(name = "t_fix_code", uniqueConstraints = { @UniqueConstraint(columnNames = "code") })
public class FixCode extends BaseEntity {

    /** codeType */
    private Integer           codeType;

    /** code */
    private Integer           code;

    /** codeName */
    private String            codeName;

    /** remark */
    private String            remark;

    /** priority */
    private Integer           priority;

    /**
     * @return the codeType
     */
    @Column(name = "CODE_TYPE")
    public Integer getCodeType() {
        return this.codeType;
    }

    /**
     * @param codeType
     *            the codeType to set
     */
    public void setCodeType(Integer codeType) {
        this.codeType = codeType;
    }

    /**
     * @return the code
     */
    @Column(name = "CODE")
    public Integer getCode() {
        return this.code;
    }

    /**
     * @param code
     *            the code to set
     */
    public void setCode(Integer code) {
        this.code = code;
    }

    /**
     * @return the codeName
     */
    @Column(name = "CODE_NAME")
    public String getCodeName() {
        return this.codeName;
    }

    /**
     * @param codeName
     *            the codeName to set
     */
    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    /**
     * @return the remark
     */
    @Column(name = "REMARK")
    public String getRemark() {
        return this.remark;
    }

    /**
     * @param remark
     *            the remark to set
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return the priority
     */
    @Column(name = "priority")
    public Integer getPriority() {
        return this.priority;
    }

    /**
     * @param priority
     *            the priority to set
     */
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    /**
     * 覆盖父类hashCode方法
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        return result;
    }

    /**
     * 覆盖父类equals方法
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) { return true; }
        if (obj == null) { return false; }
        if (getClass() != obj.getClass()) { return false; }
        FixCode other = (FixCode) obj;
        if (code == null) {
            if (other.code != null) { return false; }
        } else if (!code.equals(other.code)) { return false; }
        return true;
    }

    /**
     * 覆盖父类toString方法
     */
    @Override
    public String toString() {
        return super.toString();
    }
}
