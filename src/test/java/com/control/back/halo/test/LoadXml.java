package com.control.back.halo.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.dom4j.Document;
import org.hibernate.internal.util.xml.Origin;
import org.hibernate.internal.util.xml.XmlDocument;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * 
 * @author hongliang.dinghl SAX文档解析
 */
public class LoadXml implements XmlDocument {
    /**
     * 
     */
    private static final long serialVersionUID = 481447048653646949L;

    public void createXml(String fileName) {
        System.out.println("<<" + fileName + ">>");
    }

    public static void main(String[] args) {
        LoadXml xml = new LoadXml();
        xml.parserXml("E:\\halo_source\\halo.back.control\\master\\src\\test\\resources\\aa.xml");
    }

    public void parserXml(String fileName) {
        SAXParserFactory saxfac = SAXParserFactory.newInstance();
        try {
            SAXParser saxparser = saxfac.newSAXParser();
            InputStream is = new FileInputStream(fileName);
            saxparser.parse(is, new MySAXHandler());
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Document getDocumentTree() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Origin getOrigin() {
        // TODO Auto-generated method stub
        return null;
    }
}

class MySAXHandler extends DefaultHandler {
    boolean    hasAttribute = false;
    Attributes attributes   = null;

    public void startDocument() throws SAXException {
        System.out.println("文档开始打印了");
    }

    public void endDocument() throws SAXException {
        System.out.println("文档打印结束了");
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (attributes.getLength() > 0) {
            this.attributes = attributes;
            this.hasAttribute = true;
        }
    }

    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (hasAttribute && (attributes != null)) {
            for (int i = 0; i < attributes.getLength(); i++) {
                if (attributes.getValue(0) != null && !attributes.getValue(0).trim().equals("")) {
                    System.out.println(attributes.getQName(0) + attributes.getValue(0));
                }
            }
        }
    }

    public void characters(char[] ch, int start, int length) throws SAXException {
        if (new String(ch, start, length).trim().length() > 0) {
            System.out.println(new String(ch, start, length));
        }
    }
}
