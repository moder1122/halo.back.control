package com.control.back.halo.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.control.back.halo.SampleSpringApplication;
import com.control.back.halo.manage.dao.IAdminDao;
import com.control.back.halo.manage.entity.Admin;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = SampleSpringApplication.class)
public class ApplicationTests {

    @Autowired
    private IAdminDao adminDao;

    @Test
    public void testFind() throws Exception {
        Admin admin = adminDao.findByUsername("admin");
        System.out.println("第一次查询：" + admin.getId());

        Admin admin1 = adminDao.findByUsername("admin");
        System.out.println("第二次查询：" + admin1.getId());
    }
    
}
